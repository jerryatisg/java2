
## OpenTelemetry 

Steps: 

- run cmd to run the 3 docker container, service1, 2, and jaeger: 
    - docker compose up
- run some request to generate traffice using postman or curl cmd

```
curl -X POST --location 'localhost:8081/products' \
--header 'Content-Type: application/json' \
--data '{
    "name": "prod3",
    "description": "test3"

  }'


curl --location 'localhost:8081/products/9cd105f2-8aa6-49da-a2c1-b541dbb0a69d'
```

- open browser and browse to http://localhost:16686 which is the jeager UI
    - select service and operation. the traces and detail will be available


 
