package com.example.oteldemo2.model;

import com.example.oteldemo2.model.User;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Vehicle {

    private User user;
    private String tag;
    private String model;
}
