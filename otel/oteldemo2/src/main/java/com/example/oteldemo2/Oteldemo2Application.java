package com.example.oteldemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oteldemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(Oteldemo2Application.class, args);
    }

}
