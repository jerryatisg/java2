package com.example.oteldemo2.model;

import com.example.oteldemo2.model.Gender;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {

    private String name;

    private int age;

    private Gender gender;

    public record Ag(int age, Gender gender){ }
}
