package com.example.oteldemo2;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


import static org.junit.jupiter.api.Assertions.*;

public class CheckPalindromeTest {

    // write function to check if an input string is palindrome
    public static boolean isPalindrome(String input) {
        int length = input.length();
        for (int i = 0; i < length / 2; i++) {
            if (input.charAt(i) != input.charAt(length - i - 1)) {
                return false;
            }
        }
        return true;

    }

    public static int fib(int n){
        if( n==1 || n==2){
            return 1;
        }
        return fib(n-2) + fib(n-1);
    }

 // function to return Fibonacci sequence for a given value use dynamic programming

    public static int fibonacci(int n) {
        int fib[] = new int[n+1];
        fib[0] = 0;
        fib[1] = 1;
        for(int i=2; i<=n; i++){
            fib[i] = fib[i-1] + fib[i-2];
        }
        return fib[n];

    }

    public static boolean isaAgrams(String s1, String s2){
        char[] c1 =s1.toCharArray();

        char[] c2 = s2.toCharArray();
        Arrays.sort(c1);
        Arrays.sort(c2);
        return Arrays.equals(c1,c2);
    }


    public static int getmissing(int[] arr){
        int total  = (arr.length+2) * (arr.length+1) /2;
        int arrtotal  = Arrays.stream(arr).sum();
        return total-arrtotal;
    }

    public static boolean checkMagic(int input){
        if(input==1){
            return true;
        }
        if(input <=9){
            return false;
        }
        int red =0, inter =0;
        while(input >0){
            red += input % 10;
            input = input /10;
        }
        return checkMagic(red);
    }

    public static String rev(String input){
        if(input.length()<=1){
            return input;
        }
        char t;
        char[] c = input.toCharArray();
        for(int i=0;i<input.length()/2;i++){
            t= c[i];
            c[i] = c[input.length() - i-1];
            c[input.length()-i-1] =t;
        }
        return String.valueOf(c);
    }



    public static boolean isPrime(int input){
        if(input<=3){
            return true;
        }
        for(int i =2;i<=input/2;i++){
            if(input % i ==0){
                return false;
            }
        }
        return true;
    }

    public static int[] get2p(int input){
        int[] resu= {-1, -1};
        for(int i=2;i<=input/2; i++){
            if(isPrime(i) && isPrime(input -i)){
                return new int[]{i, input -i};
            }
        }
        return resu;
    }

    public static String rev2(String input){
        List<Character> c= input.chars().mapToObj(s -> (char)s).collect(Collectors.toList());
        Collections.reverse(c);
//        int[] r = c.stream().mapToInt(i->i).toArray();
//        Character cc = Character.valueOf('a');
//        char cc1= cc.charValue();
        return c.stream().map(String::valueOf).collect(Collectors.joining());
    }

    int[] removeElementFromArray(int[] s, int r){
        List<Integer> a = IntStream.of(s).boxed().collect(Collectors.toList());
        a.remove(r);
        return a.stream().mapToInt(i->i).toArray();
    }

    public static int bsearch(int[] d, int low, int high,  int input){
        if(d.length ==0){
            return -1;
        }
        if(low>high){
            return -1;
        }

        int mid = (low + high)/2;
        if(input == d[mid]){
            return mid;
        }
        if(input <d[mid]){
            return bsearch(d, low, mid-1, input);
        }else{
            return bsearch(d, mid+1, high, input);
        }
    }

    @Test
    void tetbsearch(){
        int[] t1 = {2, 4, 5, 8, 9, 15};
        int r = bsearch(t1, 0, t1.length-1 ,3);
        assertEquals(-1,r);
        r = bsearch(t1, 0, t1.length-1 ,2);
        assertEquals(0,r);
        r = bsearch(t1, 0, t1.length-1 ,15);
        assertEquals(5,r);
        r = bsearch(t1, 0, t1.length-1 ,5);
        assertEquals(2,r);
    }

    @Test
    void testprime(){
        assertTrue(isPrime(1));
        assertTrue(isPrime(2));
        assertTrue(isPrime(3));
        assertTrue(isPrime(5));
        assertTrue(isPrime(7));
        assertTrue(isPrime(23));
        assertFalse(isPrime(4));
        assertFalse(isPrime(6));
        assertFalse(isPrime(100));
        assertFalse(isPrime(25));
    }

    @Test
    void testget2p(){
        int[] r = get2p(18);
        Arrays.stream(r).forEach(System.out::println);
        assertTrue(r[0]!=-1);
    }

    @Test
    void testrev(){
        assertEquals("A", rev("A"));
        assertEquals("ab", rev("ba"));
        assertEquals("abc", rev("cba"));
    }

    @Test
    void testrev2(){
        assertEquals("A", rev2("A"));
        assertEquals("ab", rev2("ba"));
        assertEquals("abc", rev2("cba"));
    }

    @Test
    void testmag(){
        assertFalse(checkMagic(2));
        assertTrue(checkMagic(1));
        assertFalse(checkMagic(5));
        assertTrue(checkMagic(10));
        assertTrue(checkMagic(163));
        assertFalse(checkMagic(164));
    }
    @Test
    void testmissing(){
        int[] arr = {1,2,3,4,5};
        assertEquals(6, getmissing(arr));
        arr = new int[]{1,2,3,4,6};
        assertEquals(5, getmissing(arr));
    }
    @Test
    void tetfig(){
        assertEquals(1, fib(1));
        assertEquals(1, fib(2));
        assertEquals(2, fib(3));
        assertEquals(3, fib(4));
        assertEquals(5, fib(5));
    }

}
