package com.example.oteldemo2;

import com.example.oteldemo2.model.Gender;
import com.example.oteldemo2.model.User;
import com.example.oteldemo2.model.Vehicle;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class StreamTest {
    @Test
    void test2(){
        // sum/min/max/reduce
        List<User> users = new java.util.ArrayList<>(List.of(
                User.builder().name("Rohan").age(36).gender(Gender.M).build(),
                User.builder().name("Shashnk").age(29).gender(Gender.M).build(),
                User.builder().name("Nandni").age(29).gender(Gender.F).build(),
                User.builder().name("Shipra").age(27).gender(Gender.F).build(),
                User.builder().name("Gaurav").age(29).gender(Gender.M).build()));
        System.out.println(users.stream().map(User::getAge).reduce(0, (a, b)-> a+b));
        System.out.println(users.stream().map(User::getAge).reduce(0, (a, b)-> a-b));
        System.out.println(users.stream().map(User::getAge).reduce(0, (a, b)-> a>b?a:b));
        System.out.println(users.stream().map(User::getAge).reduce(1000, (a, b)-> a>b?b:a));
        System.out.println(users.stream().min((u1,u2)->u1.getAge()-u2.getAge()));
        System.out.println(users.stream().max(Comparator.comparingInt(User::getAge)));
        users.sort(Comparator.comparingInt(User::getAge));
        System.out.println(users.get(0));
        users.sort((u1, u2)-> u2.getAge()-u1.getAge());
        System.out.println(users.get(0));

        //group by
        Map<Gender, List<User>> result = users.stream().collect(Collectors.groupingBy(User::getGender));
        System.out.println(result.get(Gender.M).size());
        //group by: count, sum
        var result2= users.stream().collect(Collectors.groupingBy(User::getGender, Collectors.counting()));
        System.out.println(result2.get(Gender.M));
        users.stream()
                .collect(Collectors.groupingBy(User::getGender, Collectors.summingInt(User::getAge)))
                .forEach((k, v)-> System.out.println(k + ":" + v));
        users.stream()
                .collect(Collectors.groupingBy(User::getGender, Collectors.summarizingInt(User::getAge)))
                .forEach((k, v)-> System.out.println(k + ":" + v));

        //group by complex key (multiple properties) using record
        users.stream().collect(Collectors.groupingBy(u-> new User.Ag(u.getAge(), u.getGender()))).forEach((k,v)-> System.out.println(k));

        // group by multiple fields/map of map ...
        var result3 =users.stream().collect(Collectors.groupingBy(User::getGender, Collectors.groupingBy(User::getAge)));
    }

    @Test
    void testset(){
        HashSet<Object> hs = new HashSet<>();
        hs.add(null);
        hs.add(User.builder().age(100).build());
        hs.add(Vehicle.builder().tag("afdaf").build());
        assertNotNull(hs);
        TreeSet<Object> ts = new TreeSet<>();
        assertThrows(Exception.class, ()-> {
            ts.add(User.builder().age(100).build());
            ts.add(Vehicle.builder().tag("afdaf").build());
        });

    }

    static String[] getLongest (String[] inputArray) {
        int len = Arrays.stream(inputArray).max(Comparator.comparingInt(String::length)).get().length();
        return Arrays.stream(inputArray).filter(a -> a.length() == len ).toList().toArray(String[]::new);
    }


    public static String rev2(String input){
        new StringBuilder(input).reverse();

        new String(input.chars().map(a -> (char)a+1).toArray(),0, input.length());
        List<Character> c= input.chars().mapToObj(s -> (char)s).collect(Collectors.toList());
        Collections.reverse(c);
//        int[] r = c.stream().mapToInt(i->i).toArray();
//        Character cc = Character.valueOf('a');
//        char cc1= cc.charValue();
        return c.stream().map(String::valueOf).collect(Collectors.joining());
    }

    static int[] removeElementFromArray(int[] s, int r){
        Integer.toString(134);
        List<Integer> a = IntStream.of(s).boxed().collect(Collectors.toList());
        a.remove(r);
        return a.stream().mapToInt(i->i).toArray();
    }

    static String[] getLonges2 (String[] inputArray) {
        int l = Arrays.stream(inputArray).map(String::length).reduce((a, b) -> a > b ? a : b).get();
        return Arrays.stream(inputArray).filter(s-> s.length() == l).toArray(String[]::new);
    }
    static String[] getLonges3 (String[] inputArray) {
        int l = Arrays.stream(inputArray).max((a,b)-> a.length() - b.length()).get().length();
        return Arrays.stream(inputArray).filter(s-> s.length() == l).toArray(String[]::new);
    }

    int getlongest(String[] inputArray){
        return Arrays.stream(inputArray).map(String::length).max(Integer::compare).get();
    }

    void getCount(String s1){
        //count distinct chars
        s1.chars().distinct().count();
        // sc1 is map of character -> long
        var sc1 =s1.chars().mapToObj(s -> (char)s).collect(Collectors.groupingBy(Function.identity(),  Collectors.counting()));
        for(var s : sc1.entrySet()){
            long l =s.getValue();
        }
    }


    boolean s1(String inputString) {
        var m =inputString.chars().mapToObj(s -> (char)s).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        //var m= a.stream().collect(Collectors.mapping(Function.identity(), Collectors.counting()));
        int ct=0;
        for(var s : m.entrySet()){
            if(s.getValue() % 2==1){
                ct++;
            }
        }
        return ct<=1;
    }
    String stringrev(String a){
        return new StringBuilder(a).reverse().toString();
    }
    String repeatCharacter(char a, int repeat){
        return String.valueOf(a).repeat(repeat);
    }
    int getsum(int[] a){
        int max1= IntStream.of(a).boxed().reduce((c,d)-> c>d?c:d).get();
        int max2= Collections.max(IntStream.of(a).boxed().toList());
        int max3= IntStream.of(a).boxed().max(Integer::compare).get();
        int sum1= IntStream.of(a).boxed().reduce((c,d)-> c+d).get();
        return sum1;
    }

    void getCharacterCountInstr(String input){
        var m =input.chars().mapToObj(s -> (char)s).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    @Test
    void testgetlongest(){
//        String[] a = {"abc","def","hgh"};
//        String[] b = getLonges2(a);
//        assertEquals(3, b.length);
//        assertEquals(1, getLonges2(new String[]{"abc","def","high"}).length);
//        assertEquals(1, getLonges3(new String[]{"abc","def","high"}).length);
//        assertEquals(3, getLonges3(new String[]{"abc","def","hgh"}).length);
//        assertEquals(4, getlongest(new String[]{"abc","def","high"}));
        //assertEquals("bc", "abcdef".substring(1,3));
        String st = "aga";
        StringBuilder sb = new StringBuilder();
        sb.append("afa");

        String[] t= "172.16.254.1".split("\\.");
        Set<Integer> a = new HashSet<>();

        Collections.max(a);
//        assertEquals("foorabfaz", removep2main("foo(bar)faz"));
//        assertEquals("oofrabfaz", removep2main("(foo)(bar)faz"));
//        assertEquals("oof", removep2main("(foo)"));
//        assertEquals("abcqpxyzoofdef", removep2main("abc(foo(xyz)pq)def"));
    }

    boolean check(int[] d){
        HashSet<Integer> s = new HashSet<>();
        for(int i=0;i<9;i++){
            if(d[i]>9 || d[i]<1){
                return false;
            }
            if(s.contains(d[i])){
                return false;
            }
            s.add(d[i]);
        }
        return true;
    }

    @Test
    void testa(){
        int[] a = new int[]{8,3,6,5,3,6,7,2,9};
        boolean t = check(a);
        System.out.println(t);
        a=new int[]{2,7,3,6,5,7,1,9,8};
        t=check(a);
        System.out.println(t);
    }

    @Test
    void testaaa(){

        HashSet<Integer> a = new HashSet<>();
        a.add(1);
        a.add(1);
        a.add(2);
    }

    static class AnewClass{
        private int a;
        private int getA(){return a;}
        AnewClass(int a){
            this.a=a;
        }
    }

    @Test
    void testanewc(){
        AnewClass anewClass = new AnewClass(3);
        System.out.println(anewClass.getA());
    }

}
