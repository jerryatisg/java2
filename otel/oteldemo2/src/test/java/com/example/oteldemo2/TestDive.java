package com.example.oteldemo2;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDive {
    /*Given array of integers, remove each kth element from i*/
    int[] remk1(int[] inputArray, int k) {
        int[] resu = new int[inputArray.length - inputArray.length/k];
        int ct =0;
        for(int i =1; i<=inputArray.length;i++){
            if(i%k!=0 ){
                resu[ct] = inputArray[i-1];
                ct++;
            }
        }
        return resu;
    }

    int[] remk(int[] inputArray, int k) {
       return IntStream.range(0, inputArray.length).filter(i-> (i+1)%k !=0).map(i -> inputArray[i]).toArray();
    }

    char geti(String inputString){
        Matcher m = Pattern.compile("^[^0-9]*([0-9]).*$").matcher(inputString);
        return m.group(1).charAt(0);
    }
    char geti2(String inputString){
        int a='0', z='9';
        for(int i=0;i<inputString.length();i++){
            int v = inputString.charAt(i);
            if(v>=a && v<=z){
                return inputString.charAt(i);
            }
        }
        return '0';
    }
    @Test
    void testr(){
        assertEquals(new int[]{1, 2, 4, 5, 7, 8, 10}[1], remk(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 3)[1]);

        Pattern patt = Pattern.compile("^[^0-9]*([0-9]).*$");
        Matcher matcher = patt.matcher("ab01");
        if (matcher.find()) {
           System.out.println(matcher.group(1));
        }
        matcher = patt.matcher("var_1__Int");
        if(matcher.find()){
            System.out.println(matcher.group(1).charAt(0));
        }
        assertEquals('1', geti2("var_1__Int"));
    }


    @Test
    void test2(){
        String inputString="13afaf";
        String a = inputString.replaceAll("^(\\d*).*","$1");
        System.out.println(a);

        String b= "fully-qualified-domain@codesignal.com".replaceAll("^[^@]+@(.+)$", "$1");
        System.out.println(b);

        Arrays.sort(new int[]{3, 9, 2, 100});

        boolean ma = "00-1B-63-84-45-E6".matches("^[0-9A-F]{2}\\-[0-9A-F]{2}\\-[0-9A-F]{2}\\-[0-9A-F]{2}\\-[0-9A-F]{2}\\-[0-9A-F]{2}$");
        String cell ="a1";
        boolean m1= (cell.charAt(0) +2) <='h' && (cell.charAt(1) +1) <='8';
        boolean m2= (cell.charAt(0) +1) <='h' && (cell.charAt(1) +2) <='8';
        int r = ((cell.charAt(0) +2) <='h' && (cell.charAt(1) +1) <='8' ? 1 :0)
                + ((cell.charAt(0) +1) <='h' && (cell.charAt(1) +2) <='8' ? 1 :0)
                + ((cell.charAt(0) +2) <='h' && (cell.charAt(1) -1) >='1' ? 1 :0)
                + ((cell.charAt(0) +1) <='h' && (cell.charAt(1) -2) >='1' ? 1 :0)
                + ((cell.charAt(0) -2) >='a' && (cell.charAt(1) +1) <='8' ? 1 :0)
                + ((cell.charAt(0) -1) >='a' && (cell.charAt(1) + 2) <='8' ? 1 :0)
                + ((cell.charAt(0) -2) >='a' && (cell.charAt(1) -1) >='1' ? 1 :0)
                + ((cell.charAt(0) -1) >='a' && (cell.charAt(1) -2) >='1' ? 1 :0 );
        System.out.println(r);

       String s1= "10".substring(0,0);
        String s2= "10".substring(1,"10".length());

        String st3="Ready, steady, go!";
        String st4= st3.replaceAll("[^a-zA-Z]","|");
        System.out.println(st4);

        String s5 = "2 apples, 12 oranges".replaceAll("[^0-9]",",");
        System.out.println(s5);
        String s6=s5.replaceAll("(\\,){2,}","$1");
        System.out.println(s6);
    }
}
