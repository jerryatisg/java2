package com.example.oteldemo2;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class PcTest {
    private final int limit =10;

    private List<Integer> data = new ArrayList<>();


    public void produce() throws InterruptedException {
        while(true) {
            synchronized (data) {
                if(data.size()>=limit){
                    data.wait();
                }
                System.out.println(Instant.now() + "-" +Thread.currentThread().getName() +"-producing...---" + data.size() );
                data.add(ThreadLocalRandom.current().nextInt());
                Thread.sleep(ThreadLocalRandom.current().nextInt(10, 100));
                data.notify();
            }
        }
    }

    public void consume() throws InterruptedException {
        while(true) {

            synchronized (data) {
                if(data.isEmpty()){
                    data.wait();
                }
                System.out.println(Instant.now() + "-" + Thread.currentThread().getName() + "-consuming..." + data.size());
                data.remove(data.size() - 1);
                data.add(ThreadLocalRandom.current().nextInt());
                Thread.sleep(ThreadLocalRandom.current().nextInt(10, 300));
                data.notify();
            }
        }
    }

    @Test
    void testpc() throws Exception{
        Thread t1 = new Thread(() -> {
            try {produce();} catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        ExecutorService  e = Executors.newFixedThreadPool(2);
        t1.start();
        e.submit(()-> {
            try {
                consume();
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        } );
        t1.join();
        e.shutdown();
//
//        Thread t2 = new Thread(() -> {
//            try {consume();} catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        });
//        Thread t3 = new Thread(() -> {
//            try {consume();} catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        });
//        t1.start(); t2.start(); t3.start();
//
//        t1.join();t2.join();t3.join();
    }


    private static final int BUFFER_SIZE = 10;
    private BlockingQueue<Integer> buffer = new ArrayBlockingQueue<>(BUFFER_SIZE);

    public void produce2() throws InterruptedException {
        while (true) {
            int item = (int) (Math.random() * 100);
            buffer.put(item); // Add an item to the buffer
            System.out.println("Produced: " + item);
            Thread.sleep(1000); // Simulate some work
        }
    }

    public void consume2() throws InterruptedException {
        while (true) {
            int item = buffer.take(); // Remove an item from the buffer
            System.out.println("Consumed: " + item);
            Thread.sleep(1500); // Simulate some work
        }
    }

    @Test
    void test2(){
        Thread t = new Thread(()-> { try{ produce2();} catch(InterruptedException ex){ throw new RuntimeException(ex);}});
        t.start();

        Executors.newFixedThreadPool(2).submit(()-> {try {produce2();} catch (InterruptedException e) {throw new RuntimeException(e);}});

    }
}
