package com.example.oteldemo2;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestSt {

    boolean solution(String[] inputArray) {
        List<String> sl = new ArrayList<>();
        for(int i=0;i<inputArray.length; i++){
            sl.add(new String(inputArray[i]));
        }
        List<String> s2 = new ArrayList<>();
        String endStr = null;
        boolean foundone = false;
        String s = sl.get(0);
        while(sl.size()>1){

            System.out.println(s);
            foundone=false;
            for(String elem : sl){
                if(elem !=s ){
                    if(diffbyone(elem, s)){
                        s2.add(s);
                        sl.remove(s);
                        s=elem;
                        foundone=true;
                        break;
                    }
                }
            }
            if(! foundone){
                endStr = s;
                break;
            }
        }
        if(sl.size()==1){
            return true;
        }
        System.out.print("---");
        sl.addAll(s2);
        s = endStr;
        while(sl.size()>1){
            System.out.println(s);
            foundone=false;
            for(String elem : sl){
                if(elem != s ){
                    if(diffbyone(elem, s)){
                        //s2.add(s);
                        sl.remove(s);
                        s=elem;
                        foundone=true;
                        break;
                    }
                }
            }
            if(! foundone){
                return false;
            }
        }
        return sl.size()==1;
    }

    boolean diffbyone(String s1, String s2){
        int diffct=0;
        for(int i=0;i<s1.length();i++){
            if(s1.charAt(i) != s2.charAt(i)){
                diffct++;
            }
        }
        return diffct ==1;
    }


    public static boolean canRearrangeStrings(String[] strings) {
        boolean[] visited = new boolean[strings.length];
        for (int i = 0; i < strings.length; i++) {
            visited[i] = true;
            if (dfs(strings[i], strings, visited, 1)) {
                return true;
            }
            visited[i] = false;
        }
        return false;
    }

    private static boolean isOneCharApart(String str1, String str2) {
        int diffCount = 0;
        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                diffCount++;
            }
            if (diffCount > 1) {
                return false;
            }
        }
        return diffCount == 1;
    }

    private static boolean dfs(String current, String[] strings, boolean[] visited, int count) {
        if (count == strings.length) {
            return true;
        }

        for (int i = 0; i < strings.length; i++) {
            if (!visited[i] && isOneCharApart(current, strings[i])) {
                visited[i] = true;
                if (dfs(strings[i], strings, visited, count + 1)) {
                    return true;
                }
                visited[i] = false;
            }
        }

        return false;
    }
    @Test
    void test1(){
//        String[] a = {"abc", "abx", "axx", "abc"};
//        boolean b= solution(a);
//        System.out.println(b);
//        assertTrue(solution(new String[]{"zzzzab",
//                "zzzzbb",
//                "zzzzaa"}));
//        assertTrue(solution(new String[]{"abc",
//                "bef",
//                "bcc",
//                "bec",
//                "bbc",
//                "bdc"}));

        assertTrue(canRearrangeStrings(new String[]{"abc",
                "bef",
                "bcc",
                "bec",
                "bbc",
                "bdc"}));
    }
}
