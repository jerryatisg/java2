package com.example.oteldemo.controller;

import com.example.oteldemo.model.Product;
import com.example.oteldemo.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("products")
public class ProductController {
    private final ProductService service;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<Product> getAllProducts(){
        return service.getAllProducts();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public Product getProductById(@PathVariable(name = "id") UUID id ){
        var data = service.getProductDetail(id);
        return data.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find product specified."));
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Product createProduct(@RequestBody Product product){
        return service.saveProduct(product);
    }
}
