package com.example.oteldemo.controller;

import java.util.List;

// create a new restcontroller with a get endpoint
public class CatalogController {

    // a http get end point to return a list of String
    public List<String> getAllCatalog(){
        return List.of("a","b","c");
    }

    // create a http get end point to return one string
    public String getItem(String id){
        // add tracing logic here 
        return "item";
    }

    // create a http end point to return an integer
    public int getCount(){
        // add tracing logic here
        return 1;
    }

}
