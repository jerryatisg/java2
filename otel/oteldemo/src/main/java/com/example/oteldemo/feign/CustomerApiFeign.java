package com.example.oteldemo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;

@FeignClient(name="customer-api", url="http://oteldemo2:8082", path="/customers")
public interface CustomerApiFeign {
    @GetMapping(value="", produces= MediaType.APPLICATION_JSON_VALUE)
    List<String> getNames();

}
