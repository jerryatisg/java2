package com.example.oteldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OteldemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(OteldemoApplication.class, args);
    }

}
