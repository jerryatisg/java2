package com.example.oteldemo.service;

import com.example.oteldemo.feign.CustomerApiFeign;
import com.example.oteldemo.model.Product;
import com.example.oteldemo.repo.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository repository;

    private final CustomerApiFeign customerApi;

    public List<Product> getAllProducts(){
        return Streamable.of(repository.findAll()).toList();
    }

    public Product saveProduct(Product product){
        return repository.save(product);
    }

    public Optional<Product> getProductDetail(UUID id){
        var data = repository.findById(id);
        data.ifPresent(product -> product.setCustomerNames(customerApi.getNames()));
        return data;
    }
}
